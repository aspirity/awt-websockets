const STATUS_SUCCESS = 'SUCCESS';
const STATUS_ERROR = 'ERROR';
const STATUS_NOTIFICATION = 'NOTIFICATION';

class SocketController {

  private io;

  protected constructor(io) {
    this.io = io;
  }

  protected _emitSuccess(socket, action, message, id) {
    socket.emit(action, message, id);
  }

  protected _emitError(socket, action, error, id) {
    socket.emit(action, { error }, id);
  }

  protected _emitToRoom(socket, roomId, action, message) {
    this.io.to(roomId).emit(action, message);
  }
}

export default SocketController;
