import SocketController from './SocketController';
import * as v1Controllers from './v1/index';

class MainController extends SocketController {

  private controllers = {};

  public constructor(io) {
    super(io);

    for (const controllerName in v1Controllers) {
      const Controller = v1Controllers[controllerName];
      this.controllers[controllerName] = new Controller(io);
    }
  }

  public parseRequest(socket, action: string, message, id) {
    const [ controllerName, methodName ] = action.split('/');

    const controller = this.controllers[controllerName];
    if (!controller) return this._emitError(socket, action,'Controller not found', id);

    if (methodName.includes('_') || !controller[methodName])
      return this._emitError(socket, action,'Method not found', id);

    controller[methodName](socket, action, message, id);
  }
}

export default MainController;
