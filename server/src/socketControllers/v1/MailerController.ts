import SocketController from '../SocketController';

const ROOM = 'MAILER';
const ACTION_QUEUE = 'mailer/queue';
const queue = [];

class MailerController extends SocketController {

  static controllerName = 'mailer';

  public listen(socket, action, message, id) {
    socket.join(ROOM);
    this._emitSuccess(socket, ACTION_QUEUE, queue, id);
  }

  public unlisten(socket, action, message) {
    socket.leave(ROOM);
  }

  public sendMessage(socket, action, message, id) {
    if (!message) return this._emitError(socket, action, 'Data is required', id);
    if (!message.email) this._emitError(socket, action, 'Email is required', id);

    queue.push(message);
    this._emitSuccess(socket, action, true, id);
    this._emitToRoom(socket, ROOM, ACTION_QUEUE, queue);
  }
}

export default MailerController;
