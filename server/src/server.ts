import config from './config';
import Express from './Express';
import MongoDB from './MongoDB';
import Sockets from './Sockets';

const { app, server } = new Express();
const db = new MongoDB();
new Sockets(server);

db.connect();

server.listen(config.get('http.port') || 8080);

// for tests
export default app;
