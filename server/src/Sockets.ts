import * as IO from 'socket.io';
import MainController from './socketControllers/MainController';

class Sockets {
  constructor(server) {
    const io = IO(server);

    const controller = new MainController(io);

    io.on('connection', socket => {
      socket.use(([ path, message, id ], next) => {
        controller.parseRequest(socket, path, message, id);
        return next();
      });
    });
  }
}

export default Sockets;
