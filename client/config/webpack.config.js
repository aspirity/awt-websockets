const webpack = require('webpack');
const path = require('path');
const resolve = require('./webpack.config.resolve');

module.exports = {
  entry: [
    'react-hot-loader/patch',
    './src/index.jsx',
  ],
  output: {
    path: path.resolve(__dirname, 'public'),
    publicPath: '/',
    filename: 'bundle.js',
  },

  /**
   * Determine the array of extensions that should be used to resolve modules.
   */
  resolve,

  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: [
          'babel-loader',
          'eslint-loader',
        ],
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
    ],
  },
  plugins: [
    new webpack.NamedModulesPlugin(),
    new webpack.HotModuleReplacementPlugin(),
  ],
  devServer: {
    contentBase: './public',
    historyApiFallback: true,
    hot: true,
    host: '0.0.0.0',
    port: 3000,
    proxy: {
      '/api': 'http://awtwebsockets_server:8080',
      '/socket.io': {
        target: 'http://awtwebsockets_server:8080',
        ws: true,
      },
    },
  },
};
