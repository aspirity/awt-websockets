import FormProps from './FormProps';
import MailerProps from './MailerProps';

export {
  // eslint-disable-next-line import/prefer-default-export
  FormProps,
  MailerProps,
};
