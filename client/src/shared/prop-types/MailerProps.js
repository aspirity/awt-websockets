import PropTypes from 'prop-types';

const Mail = PropTypes.shape({
  email: PropTypes.string.isRequired,
  subject: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired,
});

export default {
  Mail,
};
