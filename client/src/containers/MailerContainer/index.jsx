import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Row, Col } from 'reactstrap';
import sockets from 'Sockets/index';
import { MailerProps } from 'PropTypes';
import MailerForm from './components/MailerForm';
import MailerQueue from './components/MailerQueue';

/* eslint-disable no-alert */
class MailerContainer extends Component {
  static propTypes = {
    queue: PropTypes.arrayOf(MailerProps.Mail).isRequired,
  };

  componentDidMount() {
    sockets.controller.mailer.listen();
  }

  componentWillUnmount() {
    sockets.controller.mailer.unlisten();
  }

  handleMailerFormSubmit = async (formValues) => {
    try {
      const result = await sockets.controller.mailer.sendMessage(formValues);
      alert(`Result: ${result}`);
    } catch (message) {
      alert(`Error: ${message}`);
    }
  };

  render() {
    const { queue } = this.props;
    return (
      <Row className="justify-content-center">
        <Col xs={6}>
          <h3>Mailer</h3>
          <MailerForm onSubmit={this.handleMailerFormSubmit} />
          <MailerQueue queue={queue} />
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = state => ({
  queue: state.mailer.queue,
});

// export default MailerContainer;
export default connect(mapStateToProps, {})(MailerContainer);
