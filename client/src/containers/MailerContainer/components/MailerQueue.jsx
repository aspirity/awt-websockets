import React from 'react';
import PropTypes from 'prop-types';
import { MailerProps } from 'PropTypes';
import {
  Card,
  CardBody,
} from 'reactstrap';

const MailerQueue = ({ queue }) => (
  <div>
    {queue.map((mail, i) => (
      /* eslint-disable react/no-array-index-key */
      <Card className="m-3" key={i}>
        <CardBody>
          <dl>
            <dt>Email:</dt>
            <dd>{mail.email}</dd>
          </dl>
          <dl>
            <dt>Subject:</dt>
            <dd>{mail.subject}</dd>
          </dl>
          <dl>
            <dt>Content:</dt>
            <dd>{mail.content}</dd>
          </dl>
        </CardBody>
      </Card>
    ))}
  </div>
);

MailerQueue.propTypes = {
  queue: PropTypes.arrayOf(MailerProps.Mail).isRequired,
};

export default MailerQueue;
