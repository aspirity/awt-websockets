import React from 'react';
import PropTypes from 'prop-types';
import { reduxForm } from 'redux-form';
import { validateEmail, validateLength } from 'Utils/FormValidate';
import MailerFormComponents from './MailerFormComponents';

const validate = (values) => {
  const errors = {};

  errors.email = validateEmail(values.email);
  errors.subject = validateLength(values.subject, 0, 300);
  errors.content = validateLength(values.content, 0, 10000);

  return errors;
};

const MailerForm = ({ handleSubmit, onSubmit }) => (
  <MailerFormComponents
    handleSubmit={handleSubmit}
    onSubmit={onSubmit}
  />
);

MailerForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
};

export default reduxForm({
  form: 'MailerForm',
  validate,
})(MailerForm);
