import React from 'react';
import PropTypes from 'prop-types';
import {
  Form,
  Input,
  Button,
} from 'reactstrap';
import Field from 'Shared/redux-form-components/CustomField';

const MailerFormComponents = ({ handleSubmit, onSubmit }) => (
  <Form onSubmit={handleSubmit(onSubmit)} noValidate>
    <Field
      id="email"
      name="email"
      type="email"
      component={Input}
      label="E-mail"
      placeholder="me@domain.com"
    />
    <Field
      id="subject"
      name="subject"
      type="input"
      component={Input}
      label="Subject"
      placeholder="Hello"
    />
    <Field
      id="content"
      name="content"
      type="textarea"
      component={Input}
      label="Content"
      placeholder="Blah-blah-blah"
    />
    <Button
      type="submit"
      color="primary"
      className="mr-3"
    >
      Send
    </Button>
  </Form>
);

MailerFormComponents.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
};

export default MailerFormComponents;
