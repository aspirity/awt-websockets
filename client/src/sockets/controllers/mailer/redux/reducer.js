import { handleActions } from 'redux-actions';
import {
  fetchMailerListen,
  fetchMailerQueue,
} from './actions';

const defaultState = {
  listen: false,
  queue: [],
};

export default handleActions(
  {
    [fetchMailerListen](state, { payload }) {
      return {
        ...state,
        listen: payload,
        queue: [],
      };
    },
    [fetchMailerQueue](state, { payload }) {
      return {
        ...state,
        queue: payload,
      };
    },
  },
  defaultState,
);
