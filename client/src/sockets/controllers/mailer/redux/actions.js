import { createAction } from 'redux-actions';

export const fetchMailerListen = createAction('FETCH_MAILER_LISTEN');
export const fetchMailerQueue = createAction('FETCH_MAILER_QUEUE');
