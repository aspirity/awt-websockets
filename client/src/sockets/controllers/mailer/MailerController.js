import SocketController from '../../SocketController';
import { fetchMailerQueue } from './redux/actions';

class MailerController extends SocketController {
  onQueue = this.addListener('mailer/queue', message => this.dispatch(fetchMailerQueue(message)));

  listen = this.addSender('mailer/listen');

  unlisten = this.addSender('mailer/unlisten');

  sendMessage = this.addRequester('mailer/sendMessage');
}

export default MailerController;
