import SocketController from './SocketController';
import * as Controllers from './controllers/index';

class MainController extends SocketController {
  constructor(socket, dispatch) {
    super(socket, dispatch);
    this.initControllers();
  }

  initControllers() {
    Object.entries(Controllers).forEach(([controllerName, Controller]) => {
      this[controllerName] = new Controller(this.socket, this.dispatch);
    });
  }
}

export default MainController;
