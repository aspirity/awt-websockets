class SocketController {
  constructor(socket, dispatch) {
    this.socket = socket;
    this.dispatch = dispatch;
  }

  send(action, message) {
    if (message !== undefined) {
      this.socket.emit(action, message);
    } else {
      this.socket.emit(action);
    }
  }

  addListener(action, method) {
    this.socket.on(action, method.bind(this));
  }

  addSender(action) {
    return message => this.send(action, message);
  }

  addRequester(action) {
    let id = 0;
    const requests = {};

    this.socket.on(action, (message, messageId) => {
      if (!requests[messageId]) return;
      const { resolve, reject } = requests[messageId];
      if (message.error) reject(message.error);
      else resolve(message);
    });

    return message => new Promise((resolve, reject) => {
      requests[id] = { resolve, reject };
      this.socket.emit(action, message, id);
      id += 1;
    });
  }
}

export default SocketController;
