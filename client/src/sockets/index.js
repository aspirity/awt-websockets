import io from 'socket.io-client';
import MainController from './MainController';

class Sockets {
  setDispatch(dispatch) {
    const socket = io('http://localhost:3000');
    this.controller = new MainController(socket, dispatch);
  }
}

export default new Sockets();
